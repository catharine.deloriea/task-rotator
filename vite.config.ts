import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'
import ViteYaml from '@modyfi/vite-plugin-yaml'

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  const common = { plugins: [svelte(), ViteYaml()], }
  if (command === 'build') {
    return {...common, base: '/task-rotator/'}
  }
  return common;
})
