import { writable } from 'svelte/store';

export function localStore<T>(key: string, initial: () => T, process?: (a: T) => T) {
  // receives the key of the local storage and an initial value

  const proc = (val: T) => (process ? process(val) : val);
  const toString = (value: T) => JSON.stringify(proc(value), null, 2); // helper function
  const toObj = (str: string) => proc(JSON.parse(str)); // helper function

  if (localStorage.getItem(key) === null) {
    // item not present in local storage
    localStorage.setItem(key, toString(initial())); // initialize local storage with initial value
  }

  const saved: T = toObj(localStorage.getItem(key) ?? ''); // convert to object

  const { subscribe, set } = writable<T>(saved); // create the underlying writable store
  const store = (value: T) => {
    try {
      localStorage.setItem(key, toString(value)); // save also to local storage as a string
    } catch (err) {
      console.error(err);
    }
    return set(value);
  };

  return {
    subscribe,
    set: store
    // update not working as exemplified as it calls writable's set not this one.
  };
}
