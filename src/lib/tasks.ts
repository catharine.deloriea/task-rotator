import TasksYml from '../assets/tasks.yaml';
import DailiesYml from '../assets/dailies.yaml';
import { localStore } from './localStore';
import { writable } from 'svelte/store';

export interface Task {
  task: string;
  value: number;
  currency: string;
}

/**
 * Remove any duplicate tasks preferring the first instance and preserving order.
 * @param tasks a list of tasks ((eg) from localStorage)
 * @returns the list of tasks with all duplicates removed
 */
function removeDuplicates(tasks: Task[]): Task[] {
  const names = new Set<string>();
  return tasks.filter((x) => {
    if (names.has(x.task)) {
      return false;
    }
    names.add(x.task);
    return true;
  });
}

export const AllTasks = new Set<Task>(removeDuplicates(TasksYml.map((r: Task) => r)));

/**
 * Store of the list of available tasks.
 */
export const tasks = localStore<Task[]>(
  'tasks',
  () => [...AllTasks].sort(() => 0.5 - Math.random()).slice(0, 6),
  removeDuplicates
);

export interface Daily extends Task {
  completed?: number;
}
export const dailies = writable([
  ...new Set<Daily>(removeDuplicates(DailiesYml.map((r: Task) => ({ ...r, completed: false }))))
]);
export function completeDaily(completed: Task) {
  dailies.update((l) =>
    l.map((t) => (t.task === completed.task ? { ...t, completed: Date.now() } : t))
  );
}
export function refreshDailies() {
  dailies.update((l) => l.map((t) => ({ ...t, completed: undefined })));
}
