import Tasks from '../assets/tasks.yaml';
import Dailies from '../assets/dailies.yaml';
import { localStorageStore } from '@skeletonlabs/skeleton';

export interface Wealth {
  currency: string;
  amount: number;
}
const tasks: string[] = Tasks.map((t: { currency: string }) => t.currency);
const dailies: string[] = Dailies.map((t: { currency: string }) => t.currency);
const currencies = new Set<string>([...tasks, ...dailies]);
const zeros: Wealth[] = [...currencies].map((currency) => ({ currency, amount: 0 }));

export const wealth = localStorageStore<Wealth[]>('wealth', zeros);

export const addTreasure = (treasure: string, earned: number) =>
  wealth.update((w) =>
    w.map((c) =>
      c.currency === treasure ? { currency: c.currency, amount: c.amount + earned } : c
    )
  );

/**
 * Returns a number from 1 to max.
 * @param max the max value of the die
 */
export function dieRoll(max: number): number {
  return Math.floor(Math.random() * max) + 1;
}

export function resetWealth() {
  wealth.set(zeros);
}
